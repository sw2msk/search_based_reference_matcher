package org.crossref.refmatching;

import java.util.HashMap;
import java.util.Map;
import org.crossref.common.utils.ResourceUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Dominika Tkaczyk
 * @author Joe Aparo
 */
public class CandidateTest {    
    
    private static final String TEST_FILE =
            "/api-responses/single-doi-response-1.json";
    
    private JSONObject item;
    
    @Before
    public void setup() {
        String content = ResourceUtils.readResourceAsString(TEST_FILE);
        JSONObject json = new JSONObject(content);
        item = json.getJSONObject("message").optJSONArray("items")
                .getJSONObject(0);
    }
    
    @Test
    public void testCandidateBasicFields() {
        Candidate candidate = new Candidate(item);
        candidate.setValidationScore(0.786);
        
        Assert.assertEquals(item, candidate.getItem());
        Assert.assertEquals("10.1007/s10032-015-0249-8", candidate.getDOI());
        Assert.assertEquals(127.50342, candidate.getSearchScore(), 0.0001);
        Assert.assertEquals(0.786, candidate.getValidationScore(), 0.0001);
    }
    
    @Test
    public void testUnstructuredSimilarity() {   
        item.put("score", 50);
        Candidate candidate = new Candidate(item);

        Assert.assertEquals(candidate.getStringValidationSimilarity(new Reference(
            "[1]D. Tkaczyk, P. Szostek, M. Fedoryszak, P. J. Dendek, and Ł. "
            + "Bolikowski, “CERMINE: automatic extraction of structured "
            + "metadata from scientific literature,” International Journal "
            + "on Document Analysis and Recognition (IJDAR), vol. 18, no. 4, "
            + "pp. 317–335, 2015."), 0), 0.836538, 0.0001);
        Assert.assertEquals(candidate.getStringValidationSimilarity(new Reference(
            "[1]D. Tkaczyk, P. Szostek, M. Fedoryszak, P. J. Dendek, and Ł. "
            + "Bolikowski, “CERMINE: automatic extraction of structured "
            + "metadata from scientific literature,” International Journal "
            + "on Document Analysis and Recognition (IJDAR), vol. 18, no. 4, "
            + "pp. 317–335, 2015."), 1), 0.836538, 0.0001);
        Assert.assertEquals(candidate.getStringValidationSimilarity(new Reference(
            "[1]D. Tkaczyk, P. Szostek, M. Fedoryszak, P. J. Dendek, and Ł. "
            + "Bolikowski, “CERMINE: automatic extraction of structured "
            + "metadata from scientific literature,” International Journal "
            + "on Document Analysis and Recognition (IJDAR), vol. 18, no. 4, "
            + "pp. 317–335, 2015."), 0.5), 0.836538, 0.0001);
        
        Assert.assertEquals(candidate.getStringValidationSimilarity(new Reference(
            "[1] D. Tkaczyk, P. Szostek, M. Fedoryszak, P.J. Dendek, Ł. "
            + "Bolikowski, International Journal on Document Analysis and "
            + "Recognition (IJDAR) 18 (2015) 317."), 0), 0.603618, 0.0001);
        Assert.assertEquals(candidate.getValidationSimilarity(new Reference(
            "[1]D. Tkaczyk, P. Szostek, M. Fedoryszak, P. J. Dendek, and Ł. "
            + "Bolikowski,“CERMINE: automatic extraction of structured "
            + "metadata from scientific literature,” International Journal "
            + "on Document Analysis and Recognition (IJDAR), vol. 18, no. 4, "
            + "pp. 317–335, 2015."), 0), 0.836631, 0.0001);
        
        Assert.assertEquals(candidate.getStringValidationSimilarity(new Reference(
            "[1]D. Tkaczyk and Ł. Bolikowski, “Extracting Contextual "
            + "Information from Scientific Literature Using CERMINE System,” "
            + "Communications in Computer and Information Science, pp. "
            + "93–104, 2015."), 0), 0.0, 0.0001);
        Assert.assertEquals(candidate.getStringValidationSimilarity(new Reference(
            "[1]D. Tkaczyk and Ł. Bolikowski, “Extracting Contextual "
            + "Information from Scientific Literature Using CERMINE System,” "
            + "Communications in Computer and Information Science, pp. "
            + "93–104, 2015."), 0.3), 0.102739, 0.0001);
        Assert.assertEquals(candidate.getStringValidationSimilarity(new Reference(
            "[1]D. Tkaczyk and Ł. Bolikowski, “Extracting Contextual "
            + "Information from Scientific Literature Using CERMINE System,” "
            + "Communications in Computer and Information Science, pp. "
            + "93–104, 2015."), 1), 0.342463, 0.0001);
        
        Assert.assertEquals(candidate.getStringValidationSimilarity(new Reference(
            "[1] D. Tkaczyk, Ł. Bolikowski, Communications in Computer and "
            + "Information Science (2015) 93."), 0), 0.0, 0.0001);
        Assert.assertEquals(candidate.getValidationSimilarity(new Reference(
            "[1]D. Tkaczyk and Ł. Bolikowski, “Extracting Contextual "
            + "Information from Scientific Literature Using CERMINE System,” "
            + "Communications in Computer and Information Science, pp. "
            + "93–104, 2015."), 0), 0.0, 0.0001);
        Assert.assertEquals(candidate.getValidationSimilarity(new Reference(
            "[1] D. Tkaczyk, Ł. Bolikowski, Communications in Computer and "
            + "Information Science (2015) 93."), 0), 0.0, 0.0001);
    }
    
    @Test
    public void testStructuredSimilarity() { 
        item.put("score", 50);
        Candidate candidate = new Candidate(item);

        Map<String, String> fields = new HashMap<>();
        fields.put("author", "Tkaczyk");
        fields.put("volume", "18");
        fields.put("first-page", "317");
        fields.put("year", "2015");
        fields.put("journal-title", "IJDAR");
        Reference reference = new Reference(fields);

        Assert.assertEquals(
                candidate.getStructuredValidationSimilarity(reference, 0), 0.8280,
                0.0001);
        Assert.assertEquals(
                candidate.getValidationSimilarity(reference, 0), 0.8280, 0.0001);
        
        Assert.assertEquals(
                candidate.getStructuredValidationSimilarity(reference, 0.7),
                0.8280, 0.0001);
        Assert.assertEquals(
                candidate.getValidationSimilarity(reference, 0.7), 0.8280,
                0.0001);

        fields = new HashMap<>();
        fields.put("author", "Tkaczyk");
        fields.put("volume", "93");
        fields.put("year", "2015");
        fields.put("journal-title", "Communications in Computer and Information");
        reference = new Reference(fields);

        Assert.assertEquals(
                candidate.getStructuredValidationSimilarity(reference, 0), 0.0,
                0.0001);
        Assert.assertEquals(
                candidate.getValidationSimilarity(reference, 0), 0.0, 0.0001);
        
        Assert.assertEquals(
                candidate.getStructuredValidationSimilarity(reference, 0.7),
                0.4175, 0.0001);
        Assert.assertEquals(
                candidate.getValidationSimilarity(reference, 0.7), 0.4175,
                0.0001);
    }
    
}