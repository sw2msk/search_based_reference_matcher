package org.crossref.refmatching;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.crossref.common.rest.api.ICrossRefApiClient;
import org.crossref.common.utils.LogUtils;
import org.crossref.common.utils.Timer;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Class for selecting target document candidates from the corpus.
 * 
 * @author Dominika Tkaczyk
 * @author Joe Aparo
 */
public class CandidateSelector {
    
    private final ICrossRefApiClient apiClient;
    private final Logger log = LogUtils.getLogger();
    
    public CandidateSelector(ICrossRefApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Select candidate target items.
     * 
     * @param reference The reference to match
     * @param rows The number of search items to consider as candidates
     * @param minScore The minimum relevance score to consider a search item
     * a candidate
     * @param headers Additional headers to pass in the search request
     * 
     * @return A list of candidates
     */
    public List<Candidate> findCandidates(Reference reference, int rows,
            double minScore, Map<String, String> headers) {
        String query = getSearchCandidateQuery(reference);
        
        if (StringUtils.isEmpty(query)) {
            return new ArrayList<>();
        }

        List<Candidate> candidates = searchWorks(query, rows, headers);
        return filterCandidates(query, candidates, minScore);
    }

    private List<Candidate> searchWorks(String query, int rows,
            Map<String, String> headers) {
        try {
            log.debug("API search for: " + query);
        
            Map<String, Object> args = new LinkedHashMap<>();
            args.put("rows", rows);
            args.put("query.bibliographic", query);
            
            Timer timer = new Timer();
            timer.start();
            JSONArray arr = apiClient.getWorks(args, headers);
            timer.stop();
            log.debug("apiClient.getWorks: " + timer.elapsedMs()); 
            
            List<Candidate> candidates = 
                StreamSupport.stream(arr.spliterator(), false).map(
                    item -> new Candidate((JSONObject) item))
                .collect(Collectors.toList());
            return candidates;
            
        } catch (IOException ex) {
            log.error("Error calling api client: " + ex.getMessage(), ex);
            return new ArrayList<>();
        }
    }

    public List<Candidate> filterCandidates(String query,
            List<Candidate> items, double minScore) {
        List<Candidate> candidates = new ArrayList<>();
        for (Candidate item: items) {
            if (candidates.isEmpty()) {
                candidates.add(item);
            } else if (item.getSearchScore() / query.length() >= minScore) {
                candidates.add(item);
            } else {
                break;
            }
        }
        return candidates;
    }

    public String getSearchCandidateQuery(Reference reference) {
        if (reference.getType().equals(ReferenceType.UNSTRUCTURED)) {
            return reference.getFormattedString();
        }
        StringBuilder sb = new StringBuilder(500);
        for (String key : new String[]{"author", "article-title", "journal-title",
            "series-title", "volume-title", "year", "volume", "issue",
            "first-page", "edition", "ISSN"}) {
            
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(reference.getFieldValue(key) == null ?
                    "" : reference.getFieldValue(key));
        }
        return sb.toString().replaceAll(" +", " ").trim();
    }
    
}